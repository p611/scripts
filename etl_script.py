import os
from datetime import datetime, timedelta
from time import time

import psycopg2
import pygrametl
from psycopg2 import errors
from pygrametl.datasources import SQLSource
from pygrametl.tables import Dimension, FactTable

# Chunk size to load in
isize = 1000000

# Dictionary of numbers to weekdays (Needed for the weekday attribute)
weekday_dict = {
    0: 'Monday',
    1: 'Tuesday',
    2: 'Wednesday',
    3: 'Thursday',
    4: 'Friday',
    5: 'Saturday',
    6: 'Sunday'}


# Convert timestamp to date and time smart keys
def timestamp_to_date_and_time(target_row):
    timestamp = target_row['timestamp']

    target_row['weekday'] = weekday_dict.get(datetime.weekday(timestamp))

    target_row['quarter_hour'] = int((floor_dt_quarter(timestamp)).minute / 15)

    timestamp_split = str(timestamp).split(' ')
    target_row['date_id'] = int(timestamp_split[0].replace('-', ''))
    target_row['time_id'] = int(timestamp_split[1].replace(':', ''))


# Floor datetime to quarter of hour
def floor_dt_quarter(dt):
    return dt - (dt - datetime.min) % timedelta(minutes=15)


# Convert eta to date and time smart key equivalents
def eta_to_eta_date_and_eta_time(target_row):
    eta = target_row['eta']

    if eta is None:
        target_row['eta_date'] = -1
        target_row['eta_time'] = -1
    else:
        eta_split = str(eta).split(' ')
        target_row['eta_date'] = int(eta_split[0].replace('-', ''))
        target_row['eta_time'] = int(eta_split[1].replace(':', ''))


# Read the progress file if it exists and set i to current progress, otherwise set i to 0 i.e. there is no progress
if not os.path.isfile("progress.txt"):
    with open("progress.txt", "x") as f:
        pass

with open("progress.txt", "r") as f:
    if f.readline() != "":
        f.seek(0, 0)
        nrow = int(f.readline())
    else:
        nrow = 0

# Capture current timestamp for reporting at commits
print("Starting...")
start = time()

while True:
    # Connect to the database
    pgconn = psycopg2.connect("host='localhost' dbname='ais' user='postgres' password='9Aw7VCh'")
    pgconn_wrapper = pygrametl.ConnectionWrapper(connection=pgconn)

    # Initial query to the database
    sql_query = "SELECT *, ST_SetSRID(ST_MakePoint(longitude, latitude), 4326) AS geom " \
                "FROM cleansed.\"cleansed_ais\" " \
                "WHERE id > " + str(nrow) + " " \
                "ORDER BY id " \
                "LIMIT " + str(isize)

    # Establish database as data source (SQL)
    ais_source = SQLSource(
        connection=pgconn,
        query=sql_query)

    # Define the ship dimension
    ship_dim = Dimension(
        name='ais_star_2.ship_dim',
        key='ship_id',
        attributes=['mmsi',
                    'imo',
                    'name',
                    'callsign',
                    'ship_type',
                    'width',
                    'length',
                    'data_source_type',
                    'type_of_position_fixing_device',
                    'a',
                    'b',
                    'c',
                    'd'],
        lookupatts=['mmsi', 'imo', 'name', 'callsign'])

    # Define the mission dimension
    mission_dim = Dimension(
        name='ais_star_2.mission_dim',
        key='mission_id',
        attributes=['cargo_type', 'destination', 'eta_date', 'eta_time'],
        lookupatts=['cargo_type', 'destination', 'eta_date', 'eta_time'])

    # Define the date dimension
    date_dim = Dimension(
        name='ais_star_2.date_dim',
        key='date_id',
        attributes=['weekday'])

    # Define the time dimension
    time_dim = Dimension(
        name='ais_star_2.time_dim',
        key='time_id',
        attributes=['quarter_hour'])

    # Define the fact table
    data_point_fact = FactTable(
        name='ais_star_2.data_point_fact',
        keyrefs=['ship_id', 'mission_id', 'date_id', 'time_id'],
        measures=['rot', 'sog', 'heading', 'draught', 'geom', 'navigational_status', 'cog'])

    data_point_fact.insertsql += " ON CONFLICT DO NOTHING "

    # For each row in the source
    for row in ais_source:
        nrow += 1

        # Do the necessary conversions
        timestamp_to_date_and_time(row)
        eta_to_eta_date_and_eta_time(row)

        # Insert date id key
        if row['date_id'] != date_dim.getbykey(row)['date_id']:
            date_dim.insert(row)

        # Insert time id key
        if row['time_id'] != time_dim.getbykey(row)['time_id']:
            time_dim.insert(row)

        # Insert into mission dimension, and update key
        if row['cargo_type'] is None:
            row['cargo_type'] = "N/A"
        if row['destination'] is None:
            row['destination'] = "N/A"
        row['mission_id'] = mission_dim.ensure(row)

        # Insert into ship dimension, and update key
        if row['name'] is None:
            row['name'] = "N/A"
        if row['callsign'] is None:
            row['callsign'] = "N/A"
        row['ship_id'] = ship_dim.ensure(row)

        # Insert into fact table
        data_point_fact.insert(row)

    # Write progress to file
    with open("progress.txt", "w") as f:
        f.write(str(nrow) + '\n')

    # Commit transaction
    pgconn_wrapper.commit()

    # Report processed, skipped, inserted rows and time elapsed since start of load
    now = time()
    hours, rem = divmod(now - start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("\n Rows processed: ", isize,
          "Total rows processed: ", nrow,
          "\n Total time elapsed: ", int(hours), "hours", int(minutes), "minutes", int(seconds), "seconds")

    # Close the wrapper
    pgconn_wrapper.close()

    # Close the connection
    pgconn.close()

