SELECT 
ROW_NUMBER() OVER ( ORDER BY "timestamp" ASC ) AS "id", 
"timestamp",
type_of_mobile,
mmsi,
latitude,
longitude,
navigational_status,
rot,
sog,
cog,
heading,
imo,
callsign,
"name",
ship_type,
cargo_type,
width,
"length",
type_of_position_fixing_device,
draught,
destination,
eta,
data_source_type,
"a",
b,
"c",
d

FROM raw.v_limited

-- Filter types that are not Class A nor Class B
WHERE type_of_mobile IN ('Class A', 'Class B')
AND -- Longitude and latitude bounderies that have been chosen as our AOI
(
    ( -- longitude bounds
        (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'longitude_min_aoi') < longitude AND
        longitude < (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'longitude_max_aoi')
    )
    AND
    ( -- Latitude bounds
        (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'latitude_min_aoi') < latitude  AND
        latitude < (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'latitude_max_aoi')
    )
)
AND
( -- Reasonable SOG based on shiptype. Also acts as a ship_type filter for unintersting ship_types
    (
		(sog <= (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'sog_cargo_max') AND ship_type = 'Cargo') OR 
    	(sog <= (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'sog_military_max') AND ship_type = 'Military') OR 
    	(sog <= (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'sog_passenger_max') AND ship_type = 'Passenger') OR
		(sog <= (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'sog_tanker_max') AND ship_type = 'Tanker') OR  
		(sog <= (SELECT value_real FROM public.global_variables WHERE global_variables."key" = 'sog_other_ship_max') AND ship_type NOT IN ('Cargo', 'Military','Passenger', 'Tanker' ))
	)
)
AND
( -- MarineTraffic specified interval for valid headings
    heading >= 0 AND 
    heading <= 359
)
AND
( -- MarineTraffic specified interval for valid name length
    char_length(name) <= 20
)
AND
( -- MarineTraffic specified interval for valid destination length
    char_length(destination) <= 20
)
AND
( -- MarineTraffic specified interval for valid destination length
    draught >= 0.1 AND
    draught <= 25.5
)
AND
( -- MarineTraffic specified interval for valid ROT interval
	ROT >= 0 AND
	ROT <= 720
)
