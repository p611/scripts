#!/bin/bash

#################################################################
# Script to download a csv dataset from a server daily and      #
# import it into a containered postgresql instance              #
# Usage: "./<scriptname>.sh [YYYY] [MM] [DD]                    #
# Error code 1: Invalid parameters                              #
# Error code 2: Error downloading file                          #
# Error code 3: Error importing data into database              #
#################################################################

download_year=$1
download_month=$2
download_day=$3
date=${download_year}${download_month}${download_day}
# How many days is the data old
date_delay=3

# Error handling
max_tries=5
sleep_delay_increment=47

# Ansi color code variables  
red="\e[0;91m"
green="\e[0;92m"
reset="\e[0m"

download_ftp_link="ftp://ftp.ais.dk/ais_data/"

# Server path and database/table strings
download_folder="/home/ais/download/"
download_folder_in_container="/mnt/download/"
database_name="ais"
database_user="postgres"
data_table_name="raw.ais"


# Error handling function. Takes return code and message
error () {
	if [ -n "$2" ];
	then
		printtime_nonl
		echo -e "${red}${2}${reset}"
	fi

	# If not critical error. Continue
	if [ "$1" = "0" ];
	then
		return
	fi
	exit "$1"
}

printtime () {
	echo ["$(date +"%Y-%m-%d %X")"] "${1}"
}

printtime_nonl () {
	echo -n ["$(date +"%Y-%m-%d %X")"] "${1}"
}

printtime "Starting script"

# Check and set the date time in case a special date is needed.
if [ -n "$1" ];
then
	# Check if all parameters are present
	if [ -z "$2" ] || [ -z "$3" ];
	then 
		error 1 "Missing arguments month or day"
	# Check if its a valid date
	elif [ ! "$(date -d "$date" +%Y%m%d 2> /dev/null)" = "$date" ];
	then
		error 1 "${date} is not a valid date."
	fi
	printtime "Using input date: \"${date}\" as date."
else
	# Get current date
	date=$(date +%Y%m%d)
	# Change current date to past date to compensate delay
	date=$(date --date="${date} -${date_delay} day" +%Y%m%d)
fi

# Example file: aisdk_20210303.csv
filename="aisdk_${date%\"}.csv"
url=${download_ftp_link}${filename}

# Try downloading
try_count=1
while [ ! "$try_count" -ge "$max_tries" ];
do
	printtime "Starting download"
	echo -e "\n"
	printtime_nonl
	echo -e "Downloading file (Try: $try_count/${max_tries}):"
	echo -e "Filename:    \"${filename}\""
	echo -e "Url:         \"${url}\""
	echo -e "Path:        \"${download_folder}${filename}\"\n"

	# If the last return code is 0 (Success)
	if wget -w 60 --random-wait -nv "${url}" -O "${download_folder}""${filename}";
	then
		break
	fi
	
	# wget errored
	error 0 "Failed downloading of file \"${filename}\" on \"${url}\""
	# Try again later incrementing with the try count
	# Delays: 0min -> 47min -> 94min -> 141min -> 188min -> ...
	sleep_delay=$(($sleep_delay_increment * $((try_count - 1)) + 1)) 
	printtime "Trying again in ${sleep_delay} minutes."
	sleep $((${sleep_delay} * 60))
	
	((try_count++))
	if [ "$try_count" -ge "$max_tries" ];
	then
		error 2 "Couldn't download file ${filename} after ${try_count} tries."
	fi
done

#Downloaded file
printtime "Downloaded File"

# Import into database
printtime "Importing into the database."

# Set ON_ERROR_STOP to on to make psql return error codes on exit
sql="\set ON_ERROR_STOP on
COPY ${data_table_name} FROM '${download_folder_in_container}${filename}' DELIMITER ',' CSV HEADER;"
docker exec database /bin/bash -c "echo \"${sql}\" | psql -U ${database_user} -d ${database_name} && exit $?"
# Save the exit code for error handling but cleanup first
sql_exit_code=$?

# Cleanup
printtime "Deleting file"
rm -f ${download_folder}"${filename}"

# If SQL import errored
if [ "$sql_exit_code" -ne "0" ];
then
	error 3 "Import of CSV file into datebase failed."
fi

printtime_nonl
echo -e "${green}Imported dataset: ${filename}${reset}"
printtime_nonl
echo -e "${green}=== Finished script ===${reset}"
printtime "Imported file into database - Finished script"
exit 0
